package ficherocsv;

import java.util.List;

public abstract  class Entrada {

	private Long id;
	private String name;
	private TipoEntrada type;
	private Long parentId;
	
	public Entrada() {
		
	}
	
	public Entrada(Long id, String name,TipoEntrada type, Long parentId) {
		this.id=id;
		this.name=name;
		this.type=type;
		this.parentId=parentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TipoEntrada getType() {
		return type;
	}

	public void setType(TipoEntrada type) {
		this.type = type;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public abstract boolean esFolder();
    public abstract List<Entrada> getEntradas();
    
  
	
	@Override
	public String toString() {
		return"\n---------------------------" 
			  +"\n ID: "+id
			  +"\n Nombre: "+name
		      +"\n Type: "+type
		      +"\n Parent_id: "+parentId;
	}


	    
	
	}

