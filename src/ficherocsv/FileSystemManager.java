package ficherocsv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FileSystemManager {
	public static final String SEPARADOR = ",";

	enum Test {
		UNO, DOS, TRES
	};

	public List<Entrada> cargarDatos() {
		List<Entrada> entradas = new ArrayList<Entrada>();
		BufferedReader bufferlectura = null;
		try {

			FileReader fr = new FileReader("Structure.csv");

			bufferlectura = new BufferedReader(fr);
			String linea = bufferlectura.readLine();

			int i = 0;
			while (linea != null) {
				i++;

				String[] campos = linea.split(SEPARADOR);
				// System.out.println(Arrays.toString(campos));
				linea = bufferlectura.readLine();
				if (i == 1) {

					continue;

				}

				Long id = Long.parseLong(campos[0]);
				String name = campos[1];
				TipoEntrada type = TipoEntrada.toTipoEntrada(campos[2]);
				Long parentId = null;
				String value = campos[3];

				if (value != null && !value.trim().equals("") && !value.trim().equals("null")) {

					parentId = Long.parseLong(campos[3]);
				}

				EntradaSimple nuevaEntrada = new EntradaSimple(id, name, type, parentId);
				entradas.add(nuevaEntrada);				

			}
			
			System.out.println(entradas.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (bufferlectura != null) {
				try {
					bufferlectura.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return entradas;
	}

	public boolean guardarDatos(List<Entrada> entradas) {
		boolean datosGuardados = false;

		try {

			Conexion cn = new Conexion();
			Statement s = cn.conectar().createStatement();
			s.executeUpdate("Drop table if exists ENTRADA");
			String tablaEntrada = "CREATE TABLE IF NOT EXISTS ENTRADA "
					+ "(ID int(8), Name varchar (50), type varchar (12),	parentId int(8), PRIMARY KEY (ID)) "
					+ " ENGINE = InnoDB ";
			s.executeUpdate(tablaEntrada);
			System.out.println("Tabla creada");

			// TODO

			for (Entrada entrada : entradas) {

				/*
				 * System.out.println(entrada.getId()); System.out.println(entrada.getName());
				 * System.out.println(entrada.getType());
				 * System.out.println(entrada.getParentId());
				 */

				String insertEntrada = "INSERT INTO ENTRADA(ID, Name, type, parentId) " + "VALUES(" + entrada.getId()
						+ ", " + "'" + entrada.getName() + "', " + "'" + entrada.getType() + "', "
						+ (entrada.getParentId() != null ? entrada.getParentId() : "NULL") + ")";

				s.executeUpdate(insertEntrada);

				// String insertEntrada3=("INSERT INTO ENTRADA(type)
				// VALUES("+entrada.getType()+")");
				// s.executeUpdate(insertEntrada3);
				/*
				 * String
				 * insertEntrada4=("INSERT INTO ENTRADA(parentId) VALUES("+entrada.getParentId()
				 * +")"); s.executeUpdate(insertEntrada4);
				 */

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			datosGuardados = false;
		}
		// TODO
		if (true) {
			datosGuardados = true;
		}

		return datosGuardados;
	}

	public static void metodoRecursivo(List<Entrada> entradas, int posicion) {
		boolean terminado = false;

		if (terminado) {
		} else {
			if (posicion == entradas.size()) {

				terminado = true;
				// } else if (Test.UNO.equals(Test.UNO)) {

			} else if (entradas.get(posicion).getType().equals(TipoEntrada.FOLDER)) {
				System.out.println(entradas.get(posicion).getName());
				System.out.println("");
				metodoRecursivo(entradas, posicion + 1);
			} else if (entradas.get(posicion).getType().equals(TipoEntrada.FILE)) {
				System.out.println("\t" + entradas.get(posicion).getName());
				System.out.println("");
				metodoRecursivo(entradas, posicion + 1);

			}

		}

	}

	public List<Entrada> ordenar() {
		List<Entrada> entradas = new ArrayList<Entrada>();
		BufferedReader bufferlectura = null;
		try {

			FileReader fr = new FileReader("Structure.csv");

			bufferlectura = new BufferedReader(fr);
			String linea = bufferlectura.readLine();

			int i = 0;
			while (linea != null) {
				i++;

				String[] campos = linea.split(SEPARADOR);
				// System.out.println(Arrays.toString(campos));
				linea = bufferlectura.readLine();
				if (i == 1) {

					continue;

				}

				Long id = Long.parseLong(campos[0]);
				String name = campos[1];
				TipoEntrada type = TipoEntrada.toTipoEntrada(campos[2]);
				Long parentId = null;
				String value = campos[3];

				if (value != null && !value.trim().equals("") && !value.trim().equals("null")) {

					parentId = Long.parseLong(campos[3]);
				}

				EntradaSimple nuevaEntrada = new EntradaSimple(id, name, type, parentId);
				entradas.add(nuevaEntrada);

				Collections.sort(entradas, new Comparator<Entrada>() {
					public int compare(Entrada obj1, Entrada obj2) {
						return obj1.getName().compareTo(obj2.getName());
					}

				});

			}
			
			System.out.println(entradas.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (bufferlectura != null) {
				try {
					bufferlectura.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return entradas;
	}

}
