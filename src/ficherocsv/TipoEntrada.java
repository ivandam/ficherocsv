package ficherocsv;

public enum TipoEntrada    {
	FOLDER, FILE;
	

public static TipoEntrada toTipoEntrada(String name) {
	TipoEntrada result = null;
	switch(name.toUpperCase()) {
	case "FOLDER":
		result = TipoEntrada.FOLDER;	
		break;
	case "FILE":
		result = TipoEntrada.FILE;	
		break;
}
	return result;
}
}