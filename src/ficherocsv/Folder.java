package ficherocsv;

import java.util.ArrayList;
import java.util.List;

public class Folder extends Entrada {

	public Folder(Long id, String name, TipoEntrada type, Long parentId) {
		super(id, name, type, parentId);
		// TODO Auto-generated constructor stub
	}

	private List<Entrada> entradas = new ArrayList<>();

	@Override
	public List<Entrada> getEntradas() {
		// TODO Auto-generated method stub
		return entradas;
	}

	public void setEntradas(List<Entrada> entradas) {
		this.entradas = entradas;
	}

	public void addEntrada(Entrada e) {

		entradas.add(e);
	}

	@Override
	public boolean esFolder() {
		// TODO Auto-generated method stub
		return true;
	}

}
