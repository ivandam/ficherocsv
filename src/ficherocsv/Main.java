package ficherocsv;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		FileSystemManager fs = new FileSystemManager();
		List<Entrada> entradas = fs.cargarDatos();
		boolean datosGuardados = fs.guardarDatos(entradas);
		if (datosGuardados) {
			System.out.println("Datos guardados con �xito");
		} else {
			System.out.println("Error al guardar los datos");
		}
		System.out.println("\nPATRON COMPOSITE\n");
		Long id = (long) 100;
		String name = "\tFile nueva";
		TipoEntrada type=TipoEntrada.FILE;
		Long parentId = (long) 100;
		Entrada e1=new EntradaSimple(id,name,type,parentId);
		Long id2 = (long) 200;
		String name2 = "Folder nueva";
		TipoEntrada type2 = TipoEntrada.FOLDER;
		Long parentId2 = (long) 200;
		Folder f1=new Folder(id2,name2,type2,parentId2);
		f1.addEntrada(e1);
		recorrerFicheros(f1);
		System.out.print("\nPATH CON METODO RECURSIVO\n");
		FileSystemManager.metodoRecursivo(entradas, 0);
		System.out.print("\nARRAY ORDENADO POR NOMBRE");
		fs.ordenar();
		
	}
	public static void recorrerFicheros(Entrada entradas ) {
        
        System.out.println(entradas.getName());
        if(entradas.esFolder()) {
            for (Entrada e:entradas.getEntradas()) {
                 
                recorrerFicheros(e);
            }
        }
}
}
